import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    WebView,
    ListView
} from 'react-native';

export default class RoomOcupancy extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            title: "Occupancy",
            dataSource: ds.cloneWithRows([
                'CRMTN001, Temperature: 74.5, Humidity: 58, Illuminance: 112, Occupied: Busy',
                'CRMTN002, Temperature: 70.5, Humidity: 55, Illuminance: 0, Occupied: Busy',
                'CRMTN003, Temperature: 71.5, Humidity: 51, Illuminance: 12, Occupied: Free',
                'CRMTN004, Temperature: 73.5, Humidity: 53, Illuminance: 102, Occupied: Free',
                'CRMTN005, Temperature: 76.5, Humidity: 56, Illuminance: 90, Occupied: Busy',
            ])
        }

    }

    render() {
        return (
            <View style={{flex: 1}}>
                <WebView style={styles.container}
                         source={{uri:"https://axon-sandbox.volteo.io/axon/dashboard/home/rooms"}}
                         contentInset={{top: 0, left: 0, bottom: 500, right: 0}}
                />
                <View style={styles.container}>
                    <Text style={{fontSize:30, fontFamily:'Avenir-Light',color:"#f0f0f1", textAlign:"left"}} >Sensor | list </Text>
                    <ListView

                        dataSource={this.state.dataSource}
                        renderRow={(rowData) => <Text style={styles.text}>{rowData}</Text>}
                        renderSeparator={(sectionId, rowId) => <View key={rowId} style={styles.separator} />}
                    />
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderWidth:2,
        borderColor:"black",

    },
    text:{
        textAlign:"center",
        color:"#f0f0f1",
        fontSize:13,
        fontFamily:'Avenir-Light',
    },
    map:{

    },
    separator: {
        flex: 1,
        height: StyleSheet.hairlineWidth,
        backgroundColor: '#8E8E8E',
    },
})