import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Alert,
    TextInput,
    TouchableHighlight
} from 'react-native';

export default class Template extends Component {
    constructor(props){
        super(props);
        this.state= {
            title: this.props.title,
            content: this.props.content
        }
    }
    render(){
        return(
        <View style={styles.container}>
            <View style={styles.navigationContainer}>
                <Image style={styles.navigationLogo} source={{uri: 'https://axon-sandbox.volteo.io/axon/dashboard/static/bcb98013727e6cb5d4a01b9c57524bd8.png'}}
                />
                <View ><Text style={styles.text}> {this.state.title} </Text></View>
                <Image style={styles.navigationMenu} source={require('../menuButton.png')}
                />
            </View>
            <View style={styles.content}>
                {this.state.content}
            </View>
        </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection:'column',
        backgroundColor:"black",
    },
    navigationContainer:{
        flex: 1,
        backgroundColor: '#2a3039',
        flexDirection:"row",
    },
    content:{
        flex: 7,
        backgroundColor: '#15191f',
    },
    navigationLogo:{
        width: undefined,
        height: undefined,
        resizeMode:'contain',
        flex:3.75,
        marginTop:10,
        marginBottom:10,
        marginLeft:6,
    },
    navigationMenu:{
        flex:1.5,
        width: undefined,
        height: undefined,
        marginTop:12,
        marginBottom:4,

    },
    text:{
        flex:4,
        alignItems:"center",
        justifyContent:"center",
        color:"#f0f0f1",
        fontSize:30,
        marginTop:25,
        marginRight:10,
        fontFamily:'Avenir-Light',
    }
})