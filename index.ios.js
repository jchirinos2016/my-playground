
import React, { Component } from 'react';
import {
    AppRegistry,
} from 'react-native';
import RoomOccupancy from "./components/RoomOcuppancy.js"
import Template from "./components/Template.js"

export default class AxonMobile extends Component {
    render() {
        return (
            <Template content={<RoomOccupancy />} title="Occupancy"/>
        );
    }
}

AppRegistry.registerComponent('AxonMobile', () => AxonMobile);
